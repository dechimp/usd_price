# TODO
instructions for setting up Coinbase API key

# US Dollar Price in BTC

Uses the 'coinbase' gem to interface with Coinbase's BTC prices. In this case, the price of the dollar is 1 divided by the price of BTC.

## Setup

Clone the project with 

`git clone`

Then...

`cd usd_price`

Install gems with Bundler:

`bundle install`


## Run

From the command line, run 'rake price' to get the latest price:

`rake price`

`0.00125 BTC`
