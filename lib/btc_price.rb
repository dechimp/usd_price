require 'coinbase'

module USDPrice
  class BTCPrice
    attr_reader :value, :time

    def initialize
      query
    end

    def query
      @time = Time.now
      coinbase = Coinbase::Client.new(ENV['COINBASE_API_KEY'])
      string_value = coinbase.sell_price(1).format
      @value = to_float(string_value)
    end

    def to_float(value)
      value.tr("$","").to_f
    end

  end
end
