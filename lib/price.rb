require_relative './btc_price.rb'

module USDPrice
  class Price
    attr_reader :value, :btc_price
    def initialize
      get_btc_price
      @value = (1 / @btc_price.to_f).round(5)
    end

    def get_btc_price
      @btc_price = BTCPrice.new.value rescue "Something went wrong getting the price from Coinbase"
      raise "BTC price is not a Numeric. (#{@btc_price.class}: #{@btc_price})" unless @btc_price.is_a? Numeric
    end
    
    def to_s
      "#{'%.5f' % @value} BTC"
    end
  end
end
