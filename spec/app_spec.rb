require 'spec_helper'

describe USDPrice::App do

  it "can get the price of the US Dollar" do
    expect(subject.price).to match /^\d\.\d{2,6}\sBTC/
  end

  it "can authorize with twitter" do
    expect{subject.authorize}.to_not raise_error
  end

  it "should have a properly formatted tweet" do
   expect(subject.tweet.message).to match /^.*US\sDollar.*\d{2,6}.*BTC/
  end

  it "can tweet the US Dollar price" do
    expect{subject.send}.to_not raise_error
  end
end
