require 'spec_helper'

describe USDPrice do

  context 'BTCPrice' do
    before(:all) do
      @btc_price = USDPrice::BTCPrice.new
    end

    it "can query a price from Coinbase" do
      expect { @btc_price.query}.to_not raise_error
    end

    before(:all) { @btc_price.query }

    it "should have a sensible value" do
      expect(@btc_price.value).to be_a Float
    end
    it "should have a sensible timestamp" do
      expect(@btc_price.time).to be_a Time
    end
  end

  context 'Price' do
    subject { USDPrice::Price.new }
    its(:value) { should be_a Float }
  end
end
