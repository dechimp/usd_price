require 'spec_helper'

describe 'Price' do
    before(:all) do
      @price = USDPrice::Price.new 
      @btc_price = USDPrice::BTCPrice.new
    end

    subject {@price }
    its(:value) { should be_a Float }
    its(:value) { should be < 0.01 }
    its(:value) { should be > 0 }

    it "should gather the btc price" do
      expect(subject.btc_price).to be_a Float
    end

    it "should look something like '$0.00402' when you call to_s" do
      expect(@price.to_s).to match /^\d{1}\.\d{3,6}\sBTC$/
    end

    it "should not throw any errors when getting the price" do
      expect{USDPrice::Price.new}.to_not raise_error
    end

  end
